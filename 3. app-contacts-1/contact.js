// core module (file system)
const fs = require('fs');

// core module (readline)
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


const dirData = './data';
if (!fs.existsSync(dirData)) {
    fs.mkdirSync(dirData);
}

const pathFile = './data/contacts.json';
if (!fs.existsSync(pathFile)) { 
    fs.writeFileSync(pathFile, '[]', 'utf-8');
}

const tulisPertanyaan = (pertanyaan) => {
    return new Promise((resolve, reject) => {
        rl.question(pertanyaan, (nama) => {
            resolve(nama);
        });
    });
}

const simpanContact = (nama, email, noHp) => {
    const contact = { nama, email, noHp };

    const fileBuffer = fs.readFileSync('data/contacts.json', 'utf-8');
    const contacts = JSON.parse(fileBuffer); //convert string ke json 
    contacts.push(contact);
    
    console.log(contacts);
    
    fs.writeFileSync('data/contacts.json', JSON.stringify(contacts)); //dari json diconvert ke string

    console.log("Terimakasih telah mengisi data :)")
    rl.close();
}

module.exports = { tulisPertanyaan, simpanContact };