const http = require('http');
const fs = require('fs');
const port = 3000;


const renderHTML = (path, res) => {
    fs.readFile(path, (err, data) =>  {
        if(err) {
            res.writeHead(404);
            res.write('Error: file not found');
        }else{
            res.write(data);
        }
        res.end(); // response berhenti
    });
}

http
    .createServer((req, res) => {
        res.writeHead(200, {
            'Content-Type' : 'text/html'
        });

        const url = req.url;

        switch(url){
            case '/contact':
                renderHTML('./contact.html', res);
                break;
            case '/about': 
                renderHTML('./about.html', res);
                break;
            default:
                renderHTML('./index.html', res);
                break;


                // res.write('<h2> Hello World </h2>');  //tulis response yg ditampilkan diweb browser
        }

    })
    .listen(port, () => {
        console.log(`Server is listening on port ${port} ...`);    
    });