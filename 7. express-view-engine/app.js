const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const app = express();
const port = 3000;

//gunakan ejs
app.set('view engine', 'ejs');  /// ----> maka ejs akan mendeteksi apakah ada file directory views
app.use(expressLayouts);

app.get('/', (req, res) => {

    const mahasiswa = [
        {
            nama: "Rizki",
            email: "rizki@gmail.com"
        },
        {
            nama: "Nanda",
            email: "nanda@gmail.com"
        },
        {
            nama: "Mustaqim",
            email: "mustaqim@gmail.com"
        }
    ];


    res.render('index', { 
        nama: "Rizki Nanda Mustaqim", 
        title: "Halaman Home",
        mahasiswa,
        layout: "layouts/main-layout"
    });
});

app.get('/contact', (req, res) => {
    res.render('contact', {
        title: "Halaman contact",
        layout: "layouts/main-layout"
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        title: "Halaman About",
        layout: "layouts/main-layout"
    });
});

app.get('/product/:id', (req, res) => {
    
    // contoh url : http://localhost:3000/product/7?category=shoes

    res.send(`product ID : ${req.params.id} \n <br> category : ${req.query.category}`);
});

//method use ==> untuk menghandle request lain (halaman not found)
//harus diletakkan diakhir sebuah routing
app.use('/', (req, res) => {
    res.status(404); //set status 404
    res.send('<h1>404</h1>');
})
;
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});





























