// core module (file system)
const fs = require('fs');
const chalk = require("chalk");
const validator = require("validator");
const { constants } = require('buffer');

const dirData = './data';
if (!fs.existsSync(dirData)) {
    fs.mkdirSync(dirData);
}

const pathFile = './data/contacts.json';
if (!fs.existsSync(pathFile)) { 
    fs.writeFileSync(pathFile, '[]', 'utf-8');
}

const loadContact = () => {
    // read file .json
    const fileBuffer = fs.readFileSync('data/contacts.json', 'utf-8');
    const contacts = JSON.parse(fileBuffer); //convert string ke json 
    return contacts;
}

const simpanContact = (nama, email, noHp) => {
    const contact = { nama, email, noHp };

    const contacts = loadContact();

    // cek duplikasi nama
    const duplikasi = contacts.find((contact) => contact.nama === nama);

    if(duplikasi){
        console.log(chalk `{red.inverse.bold Contact sudah terdaftar, silakan gunakan nama lain !!}`);
        return false;
    }

    // cek email
    const email_valid = validator.isEmail(email);
    if(email){
        if(!email_valid){
            console.log(chalk `{red.inverse.bold Email anda tidak valid}`);
            return false;
        }
    }

    // cek no Hp
    const noHp_valid = validator.isMobilePhone(noHp, 'id-ID');
    if(!noHp_valid){
        console.log(chalk `{red.inverse.bold No HP anda tidak valid}`);
        return false;
    }

    contacts.push(contact);    
    // console.log(contacts);
    
    fs.writeFileSync('data/contacts.json', JSON.stringify(contacts)); //dari json diconvert ke string

    console.log(chalk `{blue.inverse.bold Terimakasih telah mengisi data :)}`)
}

const listContact = () => {
    const contacts = loadContact();
    console.log(chalk `{cyan.inverse.bold Daftar Kontak : }`)
    contacts.forEach((contact, i) => {
        console.log(`${i + 1}. ${contact.nama} - ${contact.noHp}`);
    });
}

const detailContact = (nama) => {
    const contacts = loadContact();

    const contact = contacts.find((contact) => contact.nama.toLowerCase() === nama.toLowerCase()); //pencaran akan diconvert huruf kecil semua
   
    if(!contact){
        console.log(chalk `{red.inverse.bold ${nama} tidak ditemukan!}`);
        return false;
    }

    console.log(chalk `{cyan.inverse.bold ${contact.nama} }`);
    console.log(`${contact.noHp}`);

    if(contact.email){
        console.log(`${contact.email}`);
    }
}

const deleteContact = (nama) => {
    const contacts = loadContact();

    const newContacts = contacts.filter(
        (contact) => contact.nama.toLowerCase() !== nama.toLowerCase() // melakukan filter nama yg tidak dihapus akan disimpan di var newContact
    );

    if(contacts.length === newContacts.length){
        console.log(chalk `{red.inverse.bold ${nama} tidak ditemukan!}`);
        return false;
    }

    fs.writeFileSync('data/contacts.json', JSON.stringify(newContacts)); //simpan file dari json diconvert ke string
    console.log(chalk `{blue.inverse.bold data kontak ${nama} berhasil dihapus}`)
}

module.exports = { simpanContact, listContact, detailContact, deleteContact };