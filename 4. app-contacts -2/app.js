const yargs = require("yargs");
const {simpanContact, listContact, detailContact, deleteContact } = require('./contact');

// mengambil argumen dari command line 
// console.log(process.argv[2]);

/// mengambil argumen dengan module yargs ///
// console.log(yargs.argv);

yargs.command({
    command: "add",
    describe: "Menambahkan contact baru",
    builder: {
        nama: {
            describe: "Nama Lengkap",
            demandOption: true, //required / not required
            type: "string"
        },
        email: {
            describe: "Email",
            demandOption: false,
            type: 'string'
        },
        noHp: {
            describe: "Nomor Handphone",
            demandOption: true,
            type: 'string'
        }
    },
    handler: (argv) => {

        simpanContact(argv.nama, argv.email, argv.noHp);
    }    
}).demandCommand();

// Menampilkan daftar semua nama & no Hp contact
yargs.command({
    command: "list",
    describe: "Menampilkan semua nama & no Hp contact",
    handler(){
        listContact();
    },
});

// Menampilkan detail sebuah kontak
yargs.command({
    command: "detail",
    describe:"Menampilkan detail sebuah kontak berdasarkan nama",
    builder: {
        nama: {
            describe: "Nama Lengkap",
            demandOption: true,
            type: "string"
        }
    },
    handler : (argv) => {
        detailContact(argv.nama);
    }
});

// Menghapus kontak berdasarkan nama
yargs.command({
    command: "delete",
    describe: "Menghapus kontak berdasarkan nama",
    builder: {
        nama: {
            describe: "Nama Lengkap",
            demandOption: true,
            type: "string"
        }
    },
    handler: (argv) => {
        deleteContact(argv.nama);
    }
});


yargs.parse();
