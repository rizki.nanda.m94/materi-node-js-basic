const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
    // res.send('Halo dunia yang indah . . .');

    // res.json({
    //     nama: "Rizki Nanda Mustaqim",
    //     email: "rizki@gmail.com",
    //     noHp: "0121232342"
    // });

    res.sendFile('./index.html', { root: __dirname });  
});

app.get('/contact', (req, res) => {
    // res.send('Ini adalah halaman Contact . . .');
    res.sendFile('./contact.html', { root: __dirname });
});

app.get('/about', (req, res) => {
    // res.send('Ini adalah halaman About . . .');
    res.sendFile('./about.html', { root: __dirname });
});

app.get('/product/:id', (req, res) => {
    
    // contoh url : http://localhost:3000/product/7?category=shoes

    res.send(`product ID : ${req.params.id} \n <br> category : ${req.query.category}`);
});

//method use ==> untuk menghandle request lain (halaman not found)
//harus diletakkan diakhir sebuah routing
app.use('/', (req, res) => {
    res.status(404); //set status 404
    res.send('<h1>404</h1>');
})
;
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
































// const http = require('http');
// const fs = require('fs');
// const port = 3000;


// const renderHTML = (path, res) => {
//     fs.readFile(path, (err, data) =>  {
//         if(err) {
//             res.writeHead(404);
//             res.write('Error: file not found');
//         }else{
//             res.write(data);
//         }
//         res.end(); // response berhenti
//     });
// }

// http
//     .createServer((req, res) => {
//         res.writeHead(200, {
//             'Content-Type' : 'text/html'
//         });

//         const url = req.url;

//         switch(url){
//             case '/contact':
//                 renderHTML('./contact.html', res);
//                 break;
//             case '/about': 
//                 renderHTML('./about.html', res);
//                 break;
//             default:
//                 renderHTML('./index.html', res);
//                 break;


//                 // res.write('<h2> Hello World </h2>');  //tulis response yg ditampilkan diweb browser
//         }

//     })
//     .listen(port, () => {
//         console.log(`Server is listening on port ${port} ...`);    
//     });