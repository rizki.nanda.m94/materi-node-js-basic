// core module (file system)
const fs = require('fs');

// menuliskan string ke file (synchronous)
// try {
//     fs.writeFileSync('coba/test.txt', 'Hello Lala Sayang :)');
// } catch (error) {
//     console.log(error);
// }

// menulis string ke file (asynchronous)
// fs.writeFile('coba/test.txt', "Hallo dunia yg indah :D", (err) => {
//     if(err) throw err;
//     console.log('Data berhasil disimpan');
// });

// membaca file (synchronous)
// const bc = fs.readFileSync('coba/test.txt', 'utf-8');
// console.log(bc);

// membaca file (asynchronous)
// fs.readFile('coba/test.txt', 'utf-8', (err, data) => {
//     if(err) throw err;
//     console.log(data);
// })


// core module (readline)
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Masukkan nama anda : ', (nama) => {
    rl.question('Masukkan no hp anda : ', (noHp) => {
        const data_contact = { nama, noHp };
        const file = fs.readFileSync('coba/contacts.json', 'utf-8');
        const contacts = JSON.parse(file); //convert json ke json 
        contacts.push(data_contact);
        
        console.log(contacts);
        
        fs.writeFileSync('coba/contacts.json', JSON.stringify(contacts)); //dari json diconvert ke string

        rl.close();
       
    });
});
